from flask import Flask, redirect, url_for, render_template
import requests
import json
import uuid
import random
import string


app = Flask(__name__)

def getSession():
	headers = {
    	'Authorization': 'Basic bWVyY2hhbnQuTUVSQ09QWTEyOjkyZDg4Mzc5MTJkMWQ3MWVmNDcwODQ4NjU4MTU5NGI4',
    	'Content-Type': 'application/json',
	}

	data = '{ "apiOperation": "CREATE_CHECKOUT_SESSION", "interaction":{ "operation":"PURCHASE" }, "order" : { "amount" : "122.0", "currency" : "AUD", "description": "Ordered goods", "id": "' +  str(uuid.uuid4().hex) + '" } }'

	response = requests.post('https://perf.gateway.mastercard.com/api/rest/version/58/merchant/MERCOPY12/session', headers=headers, data=data)
	print(response.text)
	return json.loads(response.text)['session']['id']

@app.route("/")
def home():
	s = getSession()
	print(s)
	return render_template("index.html", session=s)

if __name__ == "__main__":
	app.run()